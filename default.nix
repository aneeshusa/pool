{ pkgs ? import <nixpkgs> {}}:

with pkgs;

runCommand "pool" { src = lib.cleanSource ./.; } ''
  mkdir -p "$out"
  cp -r "$src"/*.html "$out"
  cp -r "$src"/*.css "$out"
  cp -r "$src"/*.png "$out"
''
